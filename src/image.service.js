

export const getImages = (page) => {
    return [
        {
            'image': './images/examples/aal.jpg',
            'id': 1,
            'tags': 'Test1'
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 2,
            'tags': 'Test2'
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 3,
            'tags': 'Test3'
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 4,
            'tags': 'Test4'
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 5,
            'tags': 'Test5'
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 6,
            'tags': 'Test6'
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 7,
            'tags': ''
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 8,
            'tags': ''
        },
        {
            'image': './images/examples/aal.jpg',
            'id': 9,
            'tags': ''
        },
    ];
}