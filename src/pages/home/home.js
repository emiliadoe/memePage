import './home.scss';
import { Component } from "react";
import Carousel from 'react-bootstrap/Carousel';
import 'bootstrap/dist/css/bootstrap.min.css';
import Clickedimage from '../clickedimage/clickedimage';
import axios from 'axios';

class Home extends Component {

  constructor() {
    super();
    this.state = {serviceData: null, selectedImage: null, showModal: false, filteredData: null};
  }

  componentDidMount() {
    this.loadMemes();
  }

  loadMemes() {
    axios.get('https://63c5617ff80fabd877e72781.mockapi.io/api/memes')
      .then(res => {
        this.state = {serviceData: res.data, selectedImage: this.state.selectedImage, showModal: this.state.showModal, filteredData: res.data};
        this.setState({serviceData: res.data, selectedImage: this.state.selectedImage, showModal: this.state.showModal, filteredData: res.data});
      });
  }

  openModal = imageObj => {
    this.setState({serviceData: this.state.serviceData, selectedImage: imageObj, showModal: true, filteredData: this.state.filteredData});
  };

  filterData(event) {
    const filteredData = this.state.serviceData.filter(e => e.tags.toLowerCase().includes(event.target.value.toLowerCase()));
    this.setState({serviceData: this.state.serviceData, selectedImage: this.state.selectedImage, showModal: this.state.showModal, filteredData: filteredData});
  }

  render() {
    return (
      <div className="home">
        <Carousel>
          <Carousel.Item>
            <div className="dummy-element">
              <img src="Mockup-web-slider.jpg" alt="slider1"></img>
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="dummy-element">
            <img src="Mockup-web-slider2.jpg" alt="slider2"></img>
            </div>
            <Carousel.Caption>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
        <div className="container mb-4">
          <div className="search-memes-wrapper">
            <input className="search-memes" placeholder="Suche nach Memes" onInput={e => this.filterData(e)} />
          </div>
          <div className="headliner">
              <h2>Jetzt die Memes der Woche entdecken!</h2>
          </div>
          <div className="memes-grid">
            {this.state.filteredData?.map((object, i) => {
              return <img className="memes-grid__image" key={object.id} alt="" onClick={e => this.openModal(object)} src={object.image}></img>
            })}
          </div>
        </div>
        <Clickedimage key={this.state.selectedImage?.id} show={this.state.showModal} data={this.state.selectedImage}></Clickedimage>
      </div>
    );
  }
}

export default Home;
