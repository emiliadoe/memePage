// import { Outlet, Link } from "react-router-dom";

import { Component } from "react";
import './upload.scss';
import axios from 'axios';

class Upload extends Component {

  constructor() {
    super();
    this.state = {};
  }

  onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (e) => {
        this.setState({image: e.target.result, tags: ''});
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onChangeTags = (event) => {
    this.setState({image: this.state.image, tags: event.target.value});
  }

  onUpload = () => {
    const obj = {
      id: '',
      tags: this.state.tags,
      image: ''
    }
    axios.post('https://63c5617ff80fabd877e72781.mockapi.io/api/memes', obj).then(res => {
      document.getElementById('upload-form').reset();
      this.setState({image: '', tags: '', uploaded: true});
    });
  }

  render() {
    let alert = '';
    if (this.state.uploaded) {
      alert = <div className="alert alert-success mt-2" role="alert">Erfolgreich hochgeladen!</div>;
    }
    return (
      <div className="container">
        <form id="upload-form">
          <div className="mb-2">
            <label htmlFor="file" className="me-2">Bild auswählen</label>
            <input name="file" type="file" accept="image/png, image/jpeg" onChange={this.onImageChange} />
          </div>
          <div className="image-wrapper">
            <img className="selected-image" alt="selected-img" src={this.state?.image}/>
          </div>
          <div className="mb-2">
            <label htmlFor="tags" className="me-2">Stichworte für die Suche</label>
            <input name="tags" onChange={this.onChangeTags} type="text" />
          </div>
          <button type="button" className="btn btn-secondary" onClick={this.onUpload}>Bild hochladen</button>
          {alert}
        </form>
      </div>
    )
  }
};

export default Upload;
