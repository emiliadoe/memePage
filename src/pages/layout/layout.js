import { Outlet, Link } from "react-router-dom";
import './layout.scss';

const Layout = () => {
    return (
        <div className="layout">
            <nav>
                <Link to="/">
                    <img src="Logo.jpg" alt="Logo" className="logo"></img>
                </Link>
                <ul>
                    <li><Link to="/" className="btn">Entdecken</Link></li>
                    <li><Link to="#" className="btn">Lizenz</Link></li>
                    <li>
                        <Link to="/upload" className="btn">Hochladen</Link>
                    </li>
                    <li><Link to="#" className="btn">Registrieren</Link></li>
                    <li><Link className="btn btn-secondary" to="#">Login</Link></li>
                    <li>
                    </li>
                    <li>
                    </li>
                </ul>
            </nav>

            <Outlet />
        </div>
    )
};

export default Layout;
