import "./clickedimage.scss";
import {Component, React} from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

class Clickedimage extends Component {

    constructor(props) {
        super(props);
        this.state = {data: props.data, show: props.show};
    }

    handleClose() {
        this.setState({data: null, show: false});
    }

    download() {
        var link = document.createElement('a');
        link.href = this.state.data?.image;
        link.target = "_blank";  
        link.download = 'img.jpg';
        document.body.appendChild(link);
        link.click();
    }
    
    render() {
        return (
            <div>
                <Modal className="clickedimage" show={this.state.show} onHide={() => this.handleClose()}>
                <Modal.Header closeButton>
                <Modal.Title className="title">Ganzes Bild</Modal.Title>
                    <Button className="button" variant="success" onClick={() => this.download()}>
                        Meme speichern
                    </Button>
                </Modal.Header>
                <Modal.Body><img className="image" src={this.state.data?.image} alt=''/></Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default Clickedimage