import './App.scss';
import {
  BrowserRouter,
  Routes,
Route
  } from 'react-router-dom';
import Layout from './pages/layout/layout';
import Home from './pages/home/home';
import Upload from './pages/upload/upload';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="/upload" element={<Upload />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
