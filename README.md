# Awfu_DevUps - Kür
Aktuelle Webtechnologien: Frameworks und Tools (WiSe2223)
Gruppe: DevUps
Aufgabe: Kür
Link zum Repository: https://gitlab.com/emiliadoe/memePage

## Table of Contents
1. [Ziele des Projekts](#ziele-des-projekts)
2. [Autoren](#autoren)
3. [Gruppenverteilung] (#gruppenverteilung)
4. [Installation](#installation)
5. [Start](#start-des-projekts)
6. [Konfigurationen] (#konfigurationen)
7. [Deployment] (#deployment)
8. [Tools](#tools)
9. [Probleme] (#probleme)

## Ziele des Projekts
• Erarbeitung einer Problem-/Fragestellung
• Erstellung eines DevOps-Workflows in einzelnen Schritten
• Umsetzung einer Kür - egal ob Projektumsetzung oder Vertiefung in einzelnen Schritten des DevOps

## Autoren
Vangelis Al-Sghir, Halil Bilgen, Emilia Dörschmann, Anna Laves, Mirac Tosun

## Gruppenverteilung
Planung, Mock-Ups, Pipeline: Emilia & Mirac
Frontend: Vangelis & Halil
Tests: Anna

## Installation
A little intro about the installation. 

$ git clone https://gitlab.com/emiliadoe/memePage.git
$ cd ../path/to/the/file
$ npm install
$ npm start

## Start des Projekts

`npm install`
`npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Konfigurationen

## Deployment

## Tools

## Probleme




# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).


### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
